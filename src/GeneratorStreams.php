<?php
declare(strict_types=1);

namespace Charm\Streams;

use Exception;
use Generator;

require __DIR__.'/../vendor/autoload.php';
/**
 * Class provides PHP stream reasources that receive data provided by a PHP
 * generator.
 *
 * The generator receives and returns bytes using the `yield` keyword:
 *
 *  ```
 *  function dataSource(array &$state) {
 *      yield GeneratorStreams::KIND_READ_ONLY
 *      while (true) {
 *          $data_from_fwrite = yield $data_for_fread;
 *      }
 *  }
 *  ```
 */
class GeneratorStreams extends AbstractStreamWrapper
{
    /**
     * Template for the shared state array.
     */
    public const INITIAL_STREAM_STATE = [
        'readable' => true,
        'writable' => true,
        'seekable' => false,
        'filesize' => null,         // If the generator knows the total length of the stream
        'offset' => 0,
    ];

    /**
     * Is the stream readable?
     *
     * @var bool
     */
    public $readable = true;

    /**
     * Is the stream writable?
     *
     * @var bool
     */
    public $writable = true;

    /**
     * Is the stream seekable?
     *
     * @var bool
     */
    public $seekable = false;

    /**
     * Create a stream resource.
     *
     * @return resource
     */
    public static function create(callable $generator)
    {
        /**
         * Check that we've registered the stream wrapper.
         */
        static::register();

        /**
         * Create a unique filename for the stream.
         */
        $name = static::createUniqueName();

        /**
         * Create the unique filename, so that we can open it.
         */
        static::$sources[$name] = ['generator' => $generator];

        /*
         * This will instantiate the stream managing class.
         */
        return fopen('generator-streams://'.$name, 'a+');
    }

    /**
     * Have we registered the stream wrapper?
     *
     * @var bool
     */
    protected static $registered = false;

    /**
     * Hold stream sources until we've created the stream.
     */
    protected static $sources = [];

    /**
     * Initialize and register the stream wrapper.
     *
     * @return void
     */
    protected static function register()
    {
        if (static::$registered) {
            return;
        }
        static::$registered = true;

        stream_wrapper_register('generator-streams', static::class);
    }

    protected ?Generator $generator = null;
    protected string $state = self::STATE_OPEN;
    protected int $offset = 0;
    protected ?int $filesize = null;
    protected string $outBuffer = '';
    protected string $inBuffer = '';

    public function stream_open(string $path, string $mode, int $options, ?string &$opened_path): bool
    {
        parent::stream_open($path, $mode, $options, $opened_path);
        $u = parse_url($path);
        if (!isset($u['host'])) {
            return false;
        }
        if (!isset(static::$sources[$u['host']])) {
            return false;
        }

        $info = static::$sources[$u['host']];
        unset(static::$sources[$u['host']]);
        $this->generator = $info['generator']();

        return true;
    }

    public function stream_close(): void
    {
        $this->filesize = $this->offset;
        if ($this->generator && $this->generator->valid()) {
            $this->generator->send(false);
        }
        $this->generator = null;
        $this->state = self::STATE_CLOSED;
    }

    public function stream_seek(int $offset, int $whence = \SEEK_SET): bool
    {
        return false;
    }

    public function stream_read(int $count): string
    {
        if ('' === $this->outBuffer) {
            // trigger generator when we need it, but not earlier
            $this->runGenerator();
        }
        $result = substr($this->outBuffer, 0, $count);
        $this->outBuffer = substr($this->outBuffer, $count);
        $this->offset += \strlen($result);

        return $result;
    }

    public function stream_write(string $data): int
    {
        $this->inBuffer .= $data;
        $this->runGenerator();

        return \strlen($data);
    }

    public function stream_eof(): bool
    {
        if ($this->offset === $this->filesize) {
            return true;
        }

        $this->runGenerator();

        if (self::STATE_CLOSED === $this->state && '' === $this->outBuffer) {
            return true;
        }

        return false;
    }

    /**
     * Receive data from the generator, and/or send data to the generator. Data has different
     * meanings:.
     *
     * false            No more data will arrive
     * <string>         Added to the out-buffer. Empty strings signal we're not done.
     * int >= 0         Total file length changed
     *
     * @param mixed $send
     */
    protected function runGenerator(): bool
    {
        if (self::STATE_CLOSED === $this->state) {
            return false;
        }
        if (null === $this->generator) {
            $this->state = self::STATE_CLOSED;

            return false;
        }
        while ($this->generator->valid()) {
            $bytes = $this->inBuffer;
            $this->inBuffer = '';
            $result = $this->generator->send($bytes);

            if (\is_string($result)) {
                // Generator provided some data
                $this->outBuffer .= $result;

                return true;
            } elseif (false === $result) {
                // no more data will be coming out
                $this->state = self::STATE_CLOSED;

                return false;
            } elseif (\is_int($result) && $result >= 0) {
                // total filesize changed or became known
                if (
                    (null !== $this->filesize && $result < $this->filesize) ||
                    $result < ($this->offset + \strlen($this->outBuffer))) {
                    throw new Exception("File size can't decrease and must always be greater than the number of bytes received");
                }
                $this->filesize = $result;
            }
        }

        return false;
    }

    protected static function createUniqueName(): string
    {
        return md5(random_bytes(24));
    }

    protected function log(string $message)
    {
        echo gmdate('Y-m-d H:i:s')." $message\n";
    }
}

$fp = GeneratorStreams::fromGenerator(function () {
    while (true) {
        yield 'Some bytes';
    }
});

$pipe = GeneratorStreams::fromGenerator(function () {
    $buffer = '';
    while (true) {
        $in = yield $buffer;
        $buffer = '';
        if (\is_string($in)) {
            $buffer .= $in;
        } elseif (false === $in) {
            // closing
            return;
        }
    }
});

fwrite($pipe, 'Heisann');

while (!feof($pipe)) {
    var_dump(fread($pipe, 4096));
    if (0 === mt_rand(0, 100)) {
        fwrite($pipe, '123');
    }
}
